package cat.itb.monolith.realm.dao

import cat.itb.monolith.realm.ServiceLocator
import cat.itb.monolith.realm.model.Monument
import io.realm.kotlin.Realm
import io.realm.kotlin.delete
import io.realm.kotlin.ext.asFlow
import io.realm.kotlin.ext.query
import io.realm.kotlin.query.find
import io.realm.kotlin.types.ObjectId
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class MonumentDao(val realm: Realm, private val userId: String) {

    fun listFlow(): Flow<List<Monument>> =
        realm.query<Monument>().find().asFlow().map { it.list.toList() }

    fun myMonuments(): Flow<List<Monument>> {
        val id = ServiceLocator.realmManager.realmApp.currentUser!!.id
        return realm.query<Monument>().query("owner_id == $0", id).asFlow().map { it.list.toList() }
    }

    suspend fun editMonument(id: ObjectId?, name: String, category: String, img: ByteArray?) {
        realm.write {
            val monumentFrozen = this.query<Monument>("_id == $0", id).find().first()
            val monumentOld = findLatest(monumentFrozen)
            monumentOld?.name = name
            monumentOld?.category = category
            monumentOld?.img = img
            //monumentFrozen?.img = monument.img
        }
    }

    fun deleteMonument(id: ObjectId) {
        realm.writeBlocking {
            val monumentFrozen = this.query<Monument>("_id == $0", id).find().first()
            val monument = findLatest(monumentFrozen)
            delete(monument!!)
        }
    }


    fun addMonument(name: String, category: String, img: ByteArray?, cords: String) {
        realm.writeBlocking {
            val item = Monument(
                name = name,
                category = category,
                img = img,
                cords = cords,
                owner_id = userId
            )
            copyToRealm(item)
        }
    }
}
