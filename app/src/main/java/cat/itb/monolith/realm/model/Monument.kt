package cat.itb.monolith.realm.model

import io.realm.kotlin.types.ObjectId
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey

open class Monument(
    @PrimaryKey
    var _id: ObjectId = ObjectId.create(),
    var name: String = "",
    var category: String = "",
    var img: ByteArray? = null,
    var cords: String = "",
    var owner_id: String = ""
) : RealmObject {
    constructor() : this(name = "") {}

    override fun toString() = "Monument: $name, $category, $cords, $owner_id"
}
