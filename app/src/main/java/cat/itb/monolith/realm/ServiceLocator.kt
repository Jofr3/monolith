package cat.itb.monolith.realm

import cat.itb.monolith.realm.dao.MonumentDao

object ServiceLocator {
    val realmManager = RealmManager()
    lateinit var monumentDao: MonumentDao

    fun configureRealm(){
        requireNotNull(realmManager.realm)
        val realm = realmManager.realm!!
        monumentDao = MonumentDao(realm, realmManager.realmApp.currentUser!!.id)
    }
}
