package cat.itb.monolith.realm

import cat.itb.monolith.realm.model.Monument
import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query
import io.realm.kotlin.log.LogLevel
import io.realm.kotlin.mongodb.App
import io.realm.kotlin.mongodb.AppConfiguration
import io.realm.kotlin.mongodb.Credentials
import io.realm.kotlin.mongodb.subscriptions
import io.realm.kotlin.mongodb.sync.SyncConfiguration


class RealmManager {
    val realmApp = App.create(AppConfiguration.Builder("monolith-copmv").log(LogLevel.ALL).build())
    var realm: Realm? = null

    fun loggedIn() = realmApp.currentUser?.loggedIn ?: false

    suspend fun logout() = realmApp.currentUser?.logOut()

    suspend fun register(username: String, password: String) {
        realmApp.emailPasswordAuth.registerUser(username, password)
        login(username, password)
    }

    suspend fun login(username: String, password: String) {
        val cred = Credentials.emailPassword(username, password)
        realmApp.login(cred)
        configureRealm()
    }

    suspend fun configureRealm() {
        requireNotNull(realmApp.currentUser)

        val user = realmApp.currentUser!!
        val config = SyncConfiguration.Builder(user, setOf(Monument::class))
            .initialSubscriptions { realm ->
                add(
                    realm.query<Monument>(),
                    "all items"
                )
            }
            .waitForInitialRemoteData()
            .build()
        realm = Realm.open(config)
        realm!!.subscriptions.waitForSynchronization()

        ServiceLocator.configureRealm()
    }
}
