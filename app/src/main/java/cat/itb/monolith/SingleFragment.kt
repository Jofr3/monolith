package cat.itb.monolith

import android.annotation.SuppressLint
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import cat.itb.monolith.databinding.FragmentSingleBinding
import cat.itb.monolith.mv.MonumentMV


class SingleFragment : Fragment() {
    private lateinit var binding: FragmentSingleBinding
    private val model: MonumentMV by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSingleBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        model.selectedMonument.observe(viewLifecycleOwner, Observer {
            val monument = it
            binding.monolithName.text = monument.name.capitalize()
            binding.monolithCategory.text = "(${monument.category})"

            if (monument.img != null) {
                val bmp = BitmapFactory.decodeByteArray(monument.img, 0, monument.img!!.size)
                binding.monolithPhoto.setImageBitmap(bmp)
            }

            binding.showOnMapButton.setOnClickListener {
                println(monument.cords)
                val action =
                    SingleFragmentDirections.actionFragmentSingleMenuToFragmentMapMenu(monument.cords)
                findNavController().navigate(action)
            }
        })

    }
}