package cat.itb.monolith

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ThumbnailUtils
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import cat.itb.monolith.databinding.AddMarkerFragmentBinding
import cat.itb.monolith.mv.MonumentMV
import cat.itb.monolith.mv.RealmMV
import java.io.ByteArrayOutputStream

class AddMarkFragment : Fragment() {
    private lateinit var binding: AddMarkerFragmentBinding
    private val model: MonumentMV by activityViewModels()
    private val db: RealmMV by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = AddMarkerFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("WrongThread")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args = arguments?.getString("imageCam")

        if (args != null) {
            val bmp = BitmapFactory.decodeByteArray(model.currentImageByteArray.value, 0, model.currentImageByteArray.value!!.size)
            binding.imageCapture.setImageBitmap(bmp)
        }

        binding.cameraCaptureButton.setOnClickListener {

            findNavController().navigate(R.id.fragmentCamMenu)
        }

        binding.confirmMarkButton.setOnClickListener {
            val coordinates = model.currentCords.value

            model.addMonument(
                binding.monumentNameTextView.text.toString(),
                binding.categorySpinner.selectedItem.toString(),
                model.currentImageByteArray.value,
                coordinates!!.toString()
            )
            model.currentImageByteArray.postValue(null)

            findNavController().navigate(R.id.fragmentMapMenu)
        }
    }
}