package cat.itb.monolith

import android.annotation.SuppressLint
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import androidx.lifecycle.Observer
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import cat.itb.monolith.databinding.AddMarkerFragmentBinding
import cat.itb.monolith.databinding.EditMarkerFragmentBinding
import cat.itb.monolith.mv.MonumentMV
import cat.itb.monolith.mv.RealmMV
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class EditMarkFragment : Fragment() {
    private lateinit var binding: EditMarkerFragmentBinding
    private val model: MonumentMV by activityViewModels()
    private val db: RealmMV by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = EditMarkerFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("WrongThread")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        model.currentImageByteArray.observe(viewLifecycleOwner, Observer {
            val bmp = BitmapFactory.decodeByteArray(
                model.currentImageByteArray.value,
                0,
                model.currentImageByteArray.value!!.size
            )
            binding.imageCapture.setImageBitmap(bmp)
        })

        model.currentEditMonument.observe(viewLifecycleOwner, Observer {
            val monument = model.currentEditMonument.value

            val bmp = BitmapFactory.decodeByteArray(monument?.img, 0, monument?.img!!.size)
            binding.imageCapture.setImageBitmap(bmp)

            binding.monumentNameTextView.setText(monument.name)

            val spinnerNum = when (monument.category) {
                "Monument" -> 0
                "Escultura" -> 1
                "Edifici" -> 2
                "Museu" -> 3
                "Altres" -> 4
                else -> -1
            }

            binding.categorySpinner.setSelection(spinnerNum)
        })

        binding.cameraCaptureButton.setOnClickListener {
            findNavController().navigate(R.id.fragmentCamMenu)
        }

        binding.confirmMarkButton.setOnClickListener {
            val monument = model.currentEditMonument.value

            //monument?.img = model.currentImageByteArray.value

            CoroutineScope(Dispatchers.Main).launch {
                model.editMonument(monument?._id, binding.monumentNameTextView.text.toString(), binding.categorySpinner.selectedItem.toString(), model.currentImageByteArray.value)
            }

            findNavController().navigate(R.id.fragmentMapMenu)
        }
    }
}