package cat.itb.monolith

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import cat.itb.monolith.databinding.FragmentListBinding
import cat.itb.monolith.databinding.FragmentLoginBinding
import cat.itb.monolith.mv.RealmMV
import cat.itb.monolith.realm.RealmManager
import cat.itb.monolith.realm.ServiceLocator
import kotlinx.coroutines.*

class LoginFragment : Fragment() {
    private lateinit var binding: FragmentLoginBinding
    private val model: RealmMV by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        model.loggedIn.observe(viewLifecycleOwner) { loggedIn ->
            if (loggedIn) {
                findNavController().navigate(R.id.fragmentMapMenu)
                model.loggedIn.postValue(false)
            } else {
                Toast.makeText(activity, "Credencials incorrectes", Toast.LENGTH_SHORT).show()
                binding.confirmLogin.isEnabled = true
                binding.confirmLogin.isClickable = true
            }
        }

        binding.confirmLogin.setOnClickListener {
            binding.confirmLogin.isEnabled = false
            binding.confirmLogin.isClickable = false

            model.login(
                binding.usernameInput.text.toString(),
                binding.passwordInput.text.toString()
            )
        }

        binding.registerButton.setOnClickListener {
            findNavController().navigate(R.id.fragmentRegisterMenu)
        }
    }

    override fun onResume() {
        super.onResume()
        val supportActionBar: ActionBar? = (requireActivity() as AppCompatActivity).supportActionBar
        if (supportActionBar != null) supportActionBar.hide()
    }

    override fun onStop() {
        super.onStop()
        val supportActionBar: ActionBar? = (requireActivity() as AppCompatActivity).supportActionBar
        if (supportActionBar != null) supportActionBar.show()
    }
}