package cat.itb.monolith

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import cat.itb.monolith.databinding.FragmentLoginBinding
import cat.itb.monolith.databinding.FragmentRegisterBinding
import cat.itb.monolith.mv.RealmMV

class RegisterFragment : Fragment() {
    private lateinit var binding: FragmentRegisterBinding
    private val model: RealmMV by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        model.loggedIn.observe(viewLifecycleOwner) { loggedIn ->
            if (loggedIn) {
                findNavController().navigate(R.id.fragmentMapMenu)
                model.loggedIn.postValue(false)
            } else {
                Toast.makeText(activity, "Error al registrar", Toast.LENGTH_SHORT).show()
                binding.confirmRegister.isEnabled = true
                binding.confirmRegister.isClickable = true
            }
        }

        binding.confirmRegister.setOnClickListener {
            binding.confirmRegister.isEnabled = false
            binding.confirmRegister.isClickable = false

            model.register(
                binding.usernameInput.text.toString(),
                binding.passwordInput.text.toString()
            )
        }

        binding.registerButton.setOnClickListener {
            findNavController().navigate(R.id.fragmentLoginMenu)
        }
    }

    override fun onResume() {
        super.onResume()
        val supportActionBar: ActionBar? = (requireActivity() as AppCompatActivity).supportActionBar
        if (supportActionBar != null) supportActionBar.hide()
    }

    override fun onStop() {
        super.onStop()
        val supportActionBar: ActionBar? = (requireActivity() as AppCompatActivity).supportActionBar
        if (supportActionBar != null) supportActionBar.show()
    }
}