package cat.itb.monolith

import android.annotation.SuppressLint
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import cat.itb.monolith.databinding.FragmentMySingleBinding
import cat.itb.monolith.databinding.FragmentSingleBinding
import cat.itb.monolith.mv.MonumentMV
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MySingleFragment : Fragment() {
    private lateinit var binding: FragmentMySingleBinding
    private val model: MonumentMV by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMySingleBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        model.selectedMonument.observe(viewLifecycleOwner, Observer {
            val monument = it
            binding.monolithName.text = monument.name.capitalize()
            binding.monolithCategory.text = "(${monument.category})"

            if (monument.img != null) {
                val bmp = BitmapFactory.decodeByteArray(monument.img, 0, monument.img!!.size)
                binding.monolithPhoto.setImageBitmap(bmp)
            }

            binding.showOnMapButton.setOnClickListener {
                val action = SingleFragmentDirections.actionFragmentSingleMenuToFragmentMapMenu(monument.cords)
                findNavController().navigate(action)
            }

            binding.editMonumentButton.setOnClickListener {
                model.currentImageByteArray.postValue(monument.img)
                model.currentEditMonument.postValue(monument)
                findNavController().navigate(R.id.fragmentEditMarkMenu)
            }

            binding.deleteMonumentButton.setOnClickListener {
                model.deleteMonument(monument._id)
                findNavController().navigate(R.id.fragmentMapMenu)
            }
        })

    }
}
