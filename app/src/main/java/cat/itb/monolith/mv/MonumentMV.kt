package cat.itb.monolith.mv

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import cat.itb.monolith.realm.ServiceLocator
import cat.itb.monolith.realm.model.Monument
import com.google.android.gms.maps.model.LatLng
import io.realm.kotlin.types.ObjectId

class MonumentMV : ViewModel() {
    private val monumentDao = ServiceLocator.monumentDao
    val monuments = monumentDao.listFlow().asLiveData()
    val myMonuments = monumentDao.myMonuments().asLiveData()

    fun addMonument(name: String, category: String, img: ByteArray?, cords: String) {
        monumentDao.addMonument(name, category, img, cords)
    }

    suspend fun editMonument(id: ObjectId?, name: String, category: String, img: ByteArray?) {
        monumentDao.editMonument(id, name, category, img)
    }

    fun deleteMonument(id: ObjectId) {
        monumentDao.deleteMonument(id)
    }

    val currentCords = MutableLiveData<LatLng>()

    val selectedMonument = MutableLiveData<Monument>()

    fun select(monument: Monument) {
        selectedMonument.postValue(monument)
    }

    val currentImageByteArray = MutableLiveData<ByteArray>()

    val currentEditMonument = MutableLiveData<Monument>()
}
