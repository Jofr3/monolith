package cat.itb.monolith.mv

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cat.itb.monolith.realm.ServiceLocator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RealmMV : ViewModel() {
    val loggedIn = MutableLiveData<Boolean>()

    fun setup() {
        if (ServiceLocator.realmManager.loggedIn()) {
            CoroutineScope(Dispatchers.IO).launch {
                ServiceLocator.realmManager.configureRealm()
                loggedIn.postValue(true)
            }
        } else {
            loggedIn.postValue(false)
        }
    }

    fun login(name: String, password: String) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                ServiceLocator.realmManager.login(name, password)
                loggedIn.postValue(true)
            } catch (e: Exception) {
                loggedIn.postValue(false)
            }
        }
    }

    fun register(name: String, password: String) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                ServiceLocator.realmManager.register(name, password)
                loggedIn.postValue(true)
            } catch (e: Exception) {
                loggedIn.postValue(false)
            }
        }
    }
}