package cat.itb.monolith.other

internal interface DrawerLocker {
    fun setDrawerLocked(shouldLock: Boolean)
}
