package cat.itb.monolith.other

import cat.itb.monolith.realm.model.Monument

interface OnClickListener {
    fun onClick(monument: Monument)
}
