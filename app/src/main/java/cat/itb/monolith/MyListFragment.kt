package cat.itb.monolith

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cat.itb.monolith.adapter.MyMonumentsAdapter
import cat.itb.monolith.databinding.FragmentMyListBinding
import cat.itb.monolith.mv.MonumentMV
import cat.itb.monolith.other.OnClickListener
import cat.itb.monolith.realm.model.Monument

class MyListFragment : Fragment(), OnClickListener {
    private lateinit var monumentAdapter: MyMonumentsAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private val model: MonumentMV by activityViewModels()

    private lateinit var binding: FragmentMyListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMyListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        model.myMonuments.observe(viewLifecycleOwner, Observer {
            if (model.myMonuments.value == null) {
                Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show()
            } else {
                setUpRecyclerView(model.myMonuments.value!!)
            }
        })

        binding.button.setOnClickListener {
            findNavController().navigate(R.id.fragmentListMenu)
        }
    }

    private fun setUpRecyclerView(data: List<Monument>) {
        monumentAdapter = MyMonumentsAdapter(data, this)
        binding.recyclerMyView.apply {
            this.setHasFixedSize(true)
            linearLayoutManager = LinearLayoutManager(context)
            layoutManager = linearLayoutManager
            adapter = monumentAdapter
        }
    }

    override fun onClick(monument: Monument) {
        model.select(monument)
        findNavController().navigate(R.id.fragmentMySingleMenu)
    }
}