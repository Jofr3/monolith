package cat.itb.monolith

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import cat.itb.monolith.databinding.FragmentMapBinding
import cat.itb.monolith.mv.MonumentMV
import cat.itb.monolith.mv.RealmMV
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions


class MapFragment : Fragment(), OnMapReadyCallback {
    private lateinit var binding: FragmentMapBinding
    lateinit var map: GoogleMap
    private val model: MonumentMV by activityViewModels()
    private val db: RealmMV by activityViewModels()
    private val REQUEST_CODE_LOCATION = 100

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var currentCoordinates: LatLng

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(
            requireContext(), Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    @SuppressLint("MissingPermission")
    private fun enableLocation() {
        if (!::map.isInitialized) return
        if (isLocationPermissionGranted()) {
            map.isMyLocationEnabled = true
        } else {
            println("asd")
            requestLocationPermission()
        }
    }

    private fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            Toast.makeText(
                requireContext(),
                "Ves a la pantalla de permisos de l’aplicació i habilita el de Geolocalització",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_CODE_LOCATION
            )
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>, grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CODE_LOCATION -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                map.isMyLocationEnabled = true
                fusedLocationProviderClient =  LocationServices.getFusedLocationProviderClient(requireActivity())
                getLocation()
            } else {
                Toast.makeText(
                    requireContext(), "Accepta els permisos de geolocalització", Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        println("resume")
        super.onResume()
        if (!::map.isInitialized) return
        if (!isLocationPermissionGranted()) {
            map.isMyLocationEnabled = false
            Toast.makeText(
                requireContext(), "Accepta els permisos de geolocalització", Toast.LENGTH_SHORT
            ).show()
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLocation() {
        if (isLocationPermissionGranted()) {
            fusedLocationProviderClient.lastLocation.addOnCompleteListener {
                val location = it.result
                if (location != null) {
                    currentCoordinates = LatLng(location.latitude, location.longitude)
                    createMap()
                }
            }
        } else {
            requestLocationPermission()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentMapBinding.inflate(inflater, container, false)
        fusedLocationProviderClient =  LocationServices.getFusedLocationProviderClient(requireActivity())
        getLocation()
        //createMap()
        return binding.root
    }

    private fun createMap() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }


    private var marker: Marker? = null

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        enableLocation()

        map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentCoordinates, 18f), 5000, null)

        model.monuments.observe(viewLifecycleOwner) { list ->
            list.forEach { item ->
                val parts = item.cords.split(",")
                val cords = LatLng(parts[0].substringAfter("(").toDouble(), parts[1].substringBefore(")").toDouble())
                val myMarker = MarkerOptions().position(cords).title(item.name).snippet(item.category).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE))
                map.addMarker(myMarker)
            }
        }

        val cords = arguments?.getString("cords")
        if (cords != null) {
            val parts = cords.split(",")
            val aniCords = LatLng(parts[0].substringAfter("(").toDouble(), parts[1].substringBefore(")").toDouble())
            markerAnimate(aniCords)
        }

        binding.button.setOnClickListener {
            binding.button.visibility = View.INVISIBLE
            binding.cancelButton.visibility = View.VISIBLE

            Toast.makeText(activity, "Clicka al mapa per guardar un punt", Toast.LENGTH_SHORT).show()

            map.setOnMapClickListener {

                binding.confirmButton.visibility = View.VISIBLE

                val coordinates = LatLng(it.latitude, it.longitude)
                val myMarker = MarkerOptions().position(coordinates).title("Monument")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE))
                marker = map.addMarker(myMarker)!!

                map.setOnMapClickListener(null)

                binding.confirmButton.setOnClickListener {
                    model.currentCords.postValue(coordinates)

                    marker!!.remove()
                    findNavController().navigate(R.id.fragmentAddMarkMenu)
                }
            }

            binding.cancelButton.setOnClickListener {
                marker?.remove()
                map.setOnMapClickListener(null)
                binding.button.visibility = View.VISIBLE
                binding.confirmButton.visibility = View.INVISIBLE
                it.visibility = View.INVISIBLE
            }
        }
    }

    private fun markerAnimate(coordinates: LatLng) {
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(coordinates, 17f), 3000, null
        )
    }
}