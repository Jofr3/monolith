package cat.itb.monolith.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cat.itb.monolith.R
import cat.itb.monolith.databinding.MonumentItemBinding
import cat.itb.monolith.realm.model.Monument

class MonumentAdapter(
    private val monuments: List<Monument>,
    private val listener: cat.itb.monolith.ListFragment
) :
    RecyclerView.Adapter<MonumentAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.monument_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val monument = monuments[position]
        with(holder) {
            setListener(monument)
            binding.nameTextView.text = monument.name.capitalize()
            binding.categoryTextView.text = monument.category.capitalize()
        }
    }

    override fun getItemCount(): Int {
        return monuments.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = MonumentItemBinding.bind(view)

        fun setListener(monument: Monument) {
            binding.root.setOnClickListener {
                listener.onClick(monument)
            }
        }
    }
}
